package day02fileoi;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class Day02FileOI {

    public static void main(String[] args) {

        try {
            Scanner sc = new Scanner(System.in);
            System.out.println("Please enter your name");
            String name = sc.next();
            System.out.println(name);

            int randomNum;
            Random rand = new Random();
            randomNum = 0 + rand.nextInt((10 - 0) + 1);
            System.out.println(randomNum);

            // Writing to the file
            File file = new File("filename.txt");
            if (file.createNewFile()) {
                System.out.println("File created " + file.getName());
            } else {
                System.out.println("File already exist");
            }

            FileWriter myFile = new FileWriter("filename.txt");
            for (int i = 0; i < randomNum; i++) {
                myFile.write(name + "\n");

            }
            myFile.close();

            System.out.println("Successfully wrote to the file");

            // Reading from the file 
            
            File myObj = new File("filename.txt");
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                System.out.println(data);
            }
            myReader.close();

        } catch (IOException ex) {
            System.out.println(("Error ") + ex.getMessage());
        }

    }

}
